# Ubuntu Clean Cooking Alliance 

## Project Overview

The UCCA website will provide free access to clean cooking stove designs using
locally available materials to combat Indoor Air Pollution (IAP). It will serve
as a community-driven platform where users can download instructions, provide
feedback, and contribute new designs.
